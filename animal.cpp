///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Alexander Sidelev <asidelev@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02_13_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

namespace animalfarm {
// constructor . for each new animal
   Animal::Animal() {
      cout << ".";
   }
   Animal::~Animal() {
      cout << "x";
   }




void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
}; //genderName
	

string Animal::colorName (enum Color color) {
   switch(color){
      case BLACK:    return "Black"; break;
      case RED:      return "Red"; break;
      case WHITE:    return "White"; break;
      case SILVER:   return "Silver"; break;
      case YELLOW:   return "Yellow"; break;
      case BROWN:    return "Brown"; break;
      default:       return string("Unknown"); break;
   }
}; //colorName

// utility fxns for animal factory


//values from cpprefrence ASCII table
int asciiAtoZ_min = 65;
int asciiAtoZ_max = 90;
char Animal::randoAtoZ() {
   //initiate random number in [65,90]
   //srand(time(0));
   //assign random number from [65,90] to n
   int n1;
   n1 = (rand()%(asciiAtoZ_max + 1 - asciiAtoZ_min)) + asciiAtoZ_min;
   //convert and return ASCII decimal to char
   return char(n1);
} // randoAtoZ


int ascii_a_to_z_min = 97;
int ascii_a_to_z_max = 122;

char Animal::rando_atoz(){
//   srand(time(0));
   int n2 = (rand()%(ascii_a_to_z_max + 1 - ascii_a_to_z_min)) + ascii_a_to_z_min;
   return char(n2);
   }//rando_atoz


enum Gender Animal::randomGen() {
   int n;
   n = (rand()%3);
   switch(n) {
   case 0: return MALE;
   case 1: return FEMALE;
   case 2: return UNKNOWN;
   default: return FEMALE;
   }
};


enum Color Animal::randomColor() {
   int x;
   x = (rand()%6);
   switch(x) {
   case 0: return BLACK;
   case 1: return WHITE;
   case 2: return RED;
   case 3: return SILVER;
   case 4: return YELLOW;
   case 5: return BROWN;
   default: return BLACK;
   }
};


bool Animal::randomBool() {
   int y;
   y = (rand()%2);
   return y;
}


//const float from = 0.0000;
//const float to = 100.0000;

float Animal::randomWeight(const float from, const float to) {
   float random = ((float) rand()) / (float) RAND_MAX;
   float diff = to - from;
   float r = random * diff;
   return r;
}

string Animal::randomName() {
//   srand(time(0));
   string randomName = "";
   randomName   +=  randoAtoZ();
   int i = 0;
   int length = (rand()%(9 + 1 - 4)) + 4;
   for( i = 1; i < length; i++) {
   randomName += rando_atoz();     
   }
   return randomName;
}
//   for( int i = 0; i < 25 ; i++) {
//      animalfarm::Animal* a = AnimalFactory::randomAnimal();
//      cout << a.speak() << endl; //random animals speak
//   }

//   return 0;
//} // main fxn


} // namespace animalfarm
