///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Alexander Sidelev <asidelev@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02_13_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <list>
#include <array>
#include <time.h> 
#include <bits/stdc++.h>
#include "animal_factory.hpp"
#include "animal.hpp"
#include "fish.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp" 
#include "aku.hpp"
#include "bird.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {
   srand(time(NULL));
   cout << "Welcome to Animal Farm 3" << endl;
   
//   for ( int i = 0; i < 25 ; i++) {

//   Animal* a = AnimalFactory::randomAnimal();

//   cout << a->speak() << endl;
   
//   } // For-LOOP 

   array<Animal*, 30> animalArray;
   animalArray.fill( NULL );
   //we have to add 25 animals 
   for (int i = 0 ; i < 25 ; i++) {
      animalArray[i] = AnimalFactory::randomAnimal();
   
   } // forloop
   cout << "\nArray of Animals:" << endl;
   cout << "   Is it empty:" <<  (animalArray.empty()? " true":"  false") << endl;
   cout << "   Number of Elements:  " << animalArray.size() << endl;
   cout << "   Max size:   " <<  animalArray.max_size() << endl;
   
   // possible alternative
   // auto it = animalArray.begin(); it != animalArray.end(); ++it
   
   for(Animal* animal : animalArray){
         if (animal != NULL) { // if x is non-empty then print speak() 
         cout << animal->speak() << endl;
         } //if statement
   }// forloop

   list<Animal*> animalList;
   for (int i = 0; i < 25; i++) {
      animalList.push_front(AnimalFactory::randomAnimal());
   }
   cout << "\nList of Animals:" << endl;
   cout << "   Is it empty:" <<  (animalList.empty()? " true":"  false") << endl;
   cout << "   Number of Elements:  " << animalList.size() << endl;
   cout << "   Max size:   " <<  animalList.max_size() << endl;
   
   for(Animal* animal : animalList){
      if (animal != NULL) {
         cout << animal->speak() << endl;
      } //if statement 
   }//forloop
   return 0;

}//main fxn
