///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 205  - Object Oriented Programming
///// Lab 05a - Animal Farm 2
/////
///// @file test2.cpp
///// @version 1.0
/////
///// test harnes for animalFactory class
/////
///// @author Alexander Sidelev <asidelev@hawaii.edu>
///// @brief  Lab 06a - AnimalFarm2 - EE 205 - Spr 2021
///// @date   03_22_2021
/////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <time.h>
//#include "animal.hpp"
#include "animal_factory.hpp"

using namespace std;
using namespace animalfarm;


int main() {
 //  for (int i = 0; i < 25; i++) {

      Animal* a = AnimalFactory::randomAnimal();
    
      cout << a->speak() << endl;
   
//   } //for-loop
   return 0;
}//main fxn
