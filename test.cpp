///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 205  - Object Oriented Programming
///// Lab 05a - Animal Farm 2
/////
///// @file test.cpp
///// @version 1.0
/////
///// Test harness 
/////
///// @author Alexander Sidelev <asidelev@hawaii.edu>
///// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
///// @date   03_02_2021
/////////////////////////////////////////////////////////////////////////////////
//
#include <iostream>
#include <cstdio>
#include <cstdlib> 
#include <string>
#include <time.h>
#include "animal.cpp"
#include "animal_factory.cpp"

using namespace std;
using namespace animalfarm;
// needs to be part of animal class

char rando_atoz();
char randoAtoZ();
enum Gender randoGen();
enum Gender getNewGender();
enum Color getNewColor();
bool getNewBool();
float getNewWeight();
//values from cpprefrence ASCII table
int asciiAtoZ_min = 65;
int asciiAtoZ_max = 90;


// random integer [65,90] that is then converted to capital letter A-Z 
char randoAtoZ() {
   //initiate random number in [65,90]
   srand(time(0));
   //assign random number from [65,90] to n
   int n1;
   n1 = (rand()%(asciiAtoZ_max + 1 - asciiAtoZ_min)) + asciiAtoZ_min; 
   //convert and return ASCII decimal to char
   return char(n1);
}

int ascii_a_to_z_min = 97;
int ascii_a_to_z_max = 122;

char rando_atoz(){
 //  srand(time(0));
   int n2 = (rand()%(ascii_a_to_z_max + 1 - ascii_a_to_z_min)) + ascii_a_to_z_min;
   return char(n2);
}

enum Gender randomGen(); //fxn declaration

enum Gender randomGen() {
   int n;
   n = (rand()%3);
   switch(n) { 
   case 0: return MALE;
   case 1: return FEMALE;
   case 2: return UNKNOWN;
   default: return FEMALE;
}
}

enum Color randomColor();

enum Color randomColor() {
   int x;
   x = (rand()%6);
   switch(x) {
      case 0: return BLACK;
      case 1: return WHITE;
      case 2: return RED;
      case 3: return SILVER;
      case 4: return YELLOW;
      case 5: return BROWN;
      default: return BLACK;
}
}

bool randomBool();

bool randomBool() {
   int y;
   y = (rand()%2);
   return y;
}


// change to int when doing first method
const float from = 0.0000;
const float to = 100.0000;

float randomWeight();
//float randomWeight() {
//   int w;
//   w = (rand()%(to + 1 - from)) + from;
//   return float(w);
//   }


float randomWeight() {
   const float from = 0.0000;
   const float to = 100.0000;
   float random = ((float) rand()) / (float) RAND_MAX;
   float diff = to - from;
   float r = random * diff;
   return r;
}


// test harness main fxn
int main() {
//   srand(time(0));
//   string randomName = "";
//   randomName   +=  randoAtoZ();
//   int i = 0;
//   int length = (rand()%(9 + 1 - 4)) + 4;
//   for( i = 1; i < length; i++) {
//      randomName += rando_atoz();
//   }
   
   for( int i = 0; i < 25 ; i++) {
      animalfarm::Animal* a = AnimalFactory::randomAnimal();
      cout << a.speak() << endl; //random animals speak 
   }

//   cout << randoName << endl;
//   cout << randomGen() << endl;
//   cout << randomColor() << endl;
//   cout << boolalpha << randomBool() << endl;
//   cout << randomWeight() << endl;
   return 0;
}
